<?php

namespace CloudSource\PatoCoreBundle\Controller;

use CloudSource\PatoCoreBundle\Interfaces\InitializableControllerInterface;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;

class ConfiguratorController extends Controller implements InitializableControllerInterface {

    //Rule name to access this resource
    private $aclRuleName = 'CONFIGURATOR';

    private $aclService = null;

    public function initialize() {
        //get aclService instance and register this resource
        $this->aclService = $this->get('PatoCoreBundle.AclService');
        $this->aclService->addProtectedResource($this->aclRuleName);
    }


    public function indexAction() {

        //$this->aclService->isGranted($this->aclRuleName, $this->aclService->READ);

        return $this->render('PatoCoreBundle:Configurator:index.html.twig');
    }


}