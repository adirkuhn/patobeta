<?php

namespace CloudSource\PatoCoreBundle\Controller;

use Symfony\Component\HttpFoundation\Request;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;

use CloudSource\PatoCoreBundle\Entity\Countries;
use CloudSource\PatoCoreBundle\Form\CountriesType;

/**
 * Countries controller.
 *
 */
class CountriesController extends Controller
{

    /**
     * Lists all Countries entities.
     *
     */
    public function indexAction()
    {
        $em = $this->getDoctrine()->getManager();

        $entities = $em->getRepository('PatoCoreBundle:Countries')->findAll();

        return $this->render('PatoCoreBundle:Countries:index.html.twig', array(
            'entities' => $entities,
        ));
    }
    /**
     * Creates a new Countries entity.
     *
     */
    public function createAction(Request $request)
    {
        $entity = new Countries();
        $form = $this->createCreateForm($entity);
        $form->handleRequest($request);

        if ($form->isValid()) {
            $em = $this->getDoctrine()->getManager();
            $em->persist($entity);
            $em->flush();

            return $this->redirect($this->generateUrl('countries_show', array('id' => $entity->getId())));
        }

        return $this->render('PatoCoreBundle:Countries:new.html.twig', array(
            'entity' => $entity,
            'form'   => $form->createView(),
        ));
    }

    /**
    * Creates a form to create a Countries entity.
    *
    * @param Countries $entity The entity
    *
    * @return \Symfony\Component\Form\Form The form
    */
    private function createCreateForm(Countries $entity)
    {
        $form = $this->createForm(new CountriesType(), $entity, array(
            'action' => $this->generateUrl('countries_create'),
            'method' => 'POST',
        ));

        $form->add('submit', 'submit', array('label' => 'Create'));

        return $form;
    }

    /**
     * Displays a form to create a new Countries entity.
     *
     */
    public function newAction()
    {
        $entity = new Countries();
        $form   = $this->createCreateForm($entity);

        return $this->render('PatoCoreBundle:Countries:new.html.twig', array(
            'entity' => $entity,
            'form'   => $form->createView(),
        ));
    }

    /**
     * Finds and displays a Countries entity.
     *
     */
    public function showAction($id)
    {
        $em = $this->getDoctrine()->getManager();

        $entity = $em->getRepository('PatoCoreBundle:Countries')->find($id);

        if (!$entity) {
            throw $this->createNotFoundException('Unable to find Countries entity.');
        }

        $deleteForm = $this->createDeleteForm($id);

        return $this->render('PatoCoreBundle:Countries:show.html.twig', array(
            'entity'      => $entity,
            'delete_form' => $deleteForm->createView(),        ));
    }

    /**
     * Displays a form to edit an existing Countries entity.
     *
     */
    public function editAction($id)
    {
        $em = $this->getDoctrine()->getManager();

        $entity = $em->getRepository('PatoCoreBundle:Countries')->find($id);

        if (!$entity) {
            throw $this->createNotFoundException('Unable to find Countries entity.');
        }

        $editForm = $this->createEditForm($entity);
        $deleteForm = $this->createDeleteForm($id);

        return $this->render('PatoCoreBundle:Countries:edit.html.twig', array(
            'entity'      => $entity,
            'edit_form'   => $editForm->createView(),
            'delete_form' => $deleteForm->createView(),
        ));
    }

    /**
    * Creates a form to edit a Countries entity.
    *
    * @param Countries $entity The entity
    *
    * @return \Symfony\Component\Form\Form The form
    */
    private function createEditForm(Countries $entity)
    {
        $form = $this->createForm(new CountriesType(), $entity, array(
            'action' => $this->generateUrl('countries_update', array('id' => $entity->getId())),
            'method' => 'PUT',
        ));

        $form->add('submit', 'submit', array('label' => 'Update'));

        return $form;
    }
    /**
     * Edits an existing Countries entity.
     *
     */
    public function updateAction(Request $request, $id)
    {
        $em = $this->getDoctrine()->getManager();

        $entity = $em->getRepository('PatoCoreBundle:Countries')->find($id);

        if (!$entity) {
            throw $this->createNotFoundException('Unable to find Countries entity.');
        }

        $deleteForm = $this->createDeleteForm($id);
        $editForm = $this->createEditForm($entity);
        $editForm->handleRequest($request);

        if ($editForm->isValid()) {
            $em->flush();

            return $this->redirect($this->generateUrl('countries_edit', array('id' => $id)));
        }

        return $this->render('PatoCoreBundle:Countries:edit.html.twig', array(
            'entity'      => $entity,
            'edit_form'   => $editForm->createView(),
            'delete_form' => $deleteForm->createView(),
        ));
    }
    /**
     * Deletes a Countries entity.
     *
     */
    public function deleteAction(Request $request, $id)
    {
        $form = $this->createDeleteForm($id);
        $form->handleRequest($request);

        if ($form->isValid()) {
            $em = $this->getDoctrine()->getManager();
            $entity = $em->getRepository('PatoCoreBundle:Countries')->find($id);

            if (!$entity) {
                throw $this->createNotFoundException('Unable to find Countries entity.');
            }

            $em->remove($entity);
            $em->flush();
        }

        return $this->redirect($this->generateUrl('countries'));
    }

    /**
     * Creates a form to delete a Countries entity by id.
     *
     * @param mixed $id The entity id
     *
     * @return \Symfony\Component\Form\Form The form
     */
    private function createDeleteForm($id)
    {
        return $this->createFormBuilder()
            ->setAction($this->generateUrl('countries_delete', array('id' => $id)))
            ->setMethod('DELETE')
            ->add('submit', 'submit', array('label' => 'Delete'))
            ->getForm()
        ;
    }
}
