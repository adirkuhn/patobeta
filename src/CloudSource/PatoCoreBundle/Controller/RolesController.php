<?php

namespace CloudSource\PatoCoreBundle\Controller;

use CloudSource\PatoCoreBundle\Entity\Users;
use CloudSource\PatoCoreBundle\Interfaces\InitializableControllerInterface;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;

use CloudSource\PatoCoreBundle\Entity\Roles;
use CloudSource\PatoCoreBundle\Form\RolesType;

/**
 * Roles controller.
 *
 */
class RolesController extends Controller implements InitializableControllerInterface
{
    //Rule name to access this resource
    private $aclRuleName = 'HOME';

    private $aclService = null;

    public function initialize() {
        //get aclService instance and register this resource
        $this->aclService = $this->get('PatoCoreBundle.AclService');

        $this->aclService->addProtectedResource($this->aclRuleName);
    }

    /**
     * Lists all Roles entities.
     *
     */
    public function indexAction($user_id)
    {
        $em = $this->getDoctrine()->getManager();

        $user = $em->getRepository('PatoCoreBundle:Users')->find($user_id);

        $roles = array();

        //mount user roles
        foreach($user->getRolesObj() as $userRole) {
            $roles[$userRole->getAclProtected()->getName()][] = $userRole->getPermission();
        }

        return $this->render('PatoCoreBundle:Roles:index.html.twig', [
            'roles' => $roles,
            'user_id' => $user_id,
            'username' => $user->getName(),
        ]);
    }
    /**
     * Creates a new Roles entity.
     *
     */
    public function createAction(Request $request, $user_id)
    {

        $em = $this->getDoctrine()->getManager();

        $user = $em->getRepository('PatoCoreBundle:Users')->find($user_id);

//        $entity = new Roles();
//        $form = $this->createCreateForm($entity, $user);
//        $form->handleRequest($request);

        $post = $request->request->get('cloudsource_patocorebundle_roles');

//        print_r($post); die('joana');

        //DELETE ALL USER ROLES
        foreach($user->getRolesObj() as $userRoles) {
            $em->remove($userRoles);
        }
        $em->flush();

        //ADD USER NEW ROLES
        foreach($this->aclService->getProtectedResources() as $aclProtectedResource) {
            if (array_key_exists($aclProtectedResource, $post)) {

                //get entity og protected resource
                $emAclProtectedResource = $em->getRepository('PatoCoreBundle:AclProtected')->findByName($aclProtectedResource);

                foreach ($post[$aclProtectedResource] as $permission) {
                    $role = new Roles();
                    $role->setUser($user);
                    $role->setAclProtected($emAclProtectedResource[0]);
                    $role->setPermission($permission);
                    $em->persist($role);
                }
            }
        }
        $em->flush();

        return $this->redirect($this->generateUrl('roles', array('user_id' => $user->getId())));
    }

    /**
    * Creates a form to create a Roles entity.
    *
    * @param Roles $entity The entity
    *
    * @return \Symfony\Component\Form\Form The form
    */
    private function createCreateForm(Roles $entity, Users $user)
    {

        $form = $this->createForm(new RolesType(), null, array(
            'action' => $this->generateUrl('roles_create', array('user_id' => $user->getId())),
            'method' => 'POST',
        ));



        foreach($this->aclService->getProtectedResources() as $protectedResource) {

            $form->add($protectedResource, 'choice', array(
                'mapped' => false,
                'expanded' => true,
                'multiple' => true,
                'label' => $protectedResource,
                'choices' => array(
                    $this->aclService->READ => $this->aclService->READ,
                    $this->aclService->WRITE => $this->aclService->WRITE,
                    $this->aclService->DELETE => $this->aclService->DELETE,
                ),
                'data' => $user->getRolesByAclProtectedResource($protectedResource)

            ));
        }

//        $form->add('user_roles', 'choice', array(
//            'choices' => $user->getRoles(),
//            'mapped' => false,
//            'multiple' => true,
//            'label' => 'User Roles'
//        ));

        $form->add('submit', 'submit', array('label' => 'Create'));

        return $form;
    }

    /**
     * Displays a form to create a new Roles entity.
     *
     */
    public function newAction($user_id)
    {
        $em = $this->getDoctrine()->getManager();

        $user = $em->getRepository('PatoCoreBundle:Users')->find($user_id);

        $entity = new Roles();
        $form   = $this->createCreateForm($entity, $user);

        return $this->render('PatoCoreBundle:Roles:new.html.twig', array(
            'entity' => $entity,
            'form'   => $form->createView(),
            'user_id' => $user_id,
            'username' => $user->getName()
        ));
    }

    /**
     * Finds and displays a Roles entity.
     *
     */
    public function showAction($user_id)
    {
        $em = $this->getDoctrine()->getManager();

        $entity = $em->getRepository('PatoCoreBundle:Roles')->find($id);

        if (!$entity) {
            throw $this->createNotFoundException('Unable to find Roles entity.');
        }

        $deleteForm = $this->createDeleteForm($id);

        return $this->render('PatoCoreBundle:Roles:show.html.twig', array(
            'entity'      => $entity,
            'delete_form' => $deleteForm->createView(),        ));
    }

    /**
     * Displays a form to edit an existing Roles entity.
     *
     */
    public function editAction($user_id)
    {
        $em = $this->getDoctrine()->getManager();

        $entity = $em->getRepository('PatoCoreBundle:Roles')->find($id);

        if (!$entity) {
            throw $this->createNotFoundException('Unable to find Roles entity.');
        }

        $editForm = $this->createEditForm($entity);
        $deleteForm = $this->createDeleteForm($id);

        return $this->render('PatoCoreBundle:Roles:edit.html.twig', array(
            'entity'      => $entity,
            'edit_form'   => $editForm->createView(),
            'delete_form' => $deleteForm->createView(),
        ));
    }

    /**
    * Creates a form to edit a Roles entity.
    *
    * @param Roles $entity The entity
    *
    * @return \Symfony\Component\Form\Form The form
    */
    private function createEditForm(Roles $entity)
    {
        $form = $this->createForm(new RolesType(), $entity, array(
            'action' => $this->generateUrl('roles_update', array('id' => $entity->getId())),
            'method' => 'PUT',
        ));

        $form->add('submit', 'submit', array('label' => 'Update'));

        return $form;
    }
    /**
     * Edits an existing Roles entity.
     *
     */
    public function updateAction(Request $request, $id)
    {
        $em = $this->getDoctrine()->getManager();

        $entity = $em->getRepository('PatoCoreBundle:Roles')->find($id);

        if (!$entity) {
            throw $this->createNotFoundException('Unable to find Roles entity.');
        }

        $deleteForm = $this->createDeleteForm($id);
        $editForm = $this->createEditForm($entity);
        $editForm->handleRequest($request);

        if ($editForm->isValid()) {
            $em->flush();

            return $this->redirect($this->generateUrl('roles_edit', array('id' => $id)));
        }

        return $this->render('PatoCoreBundle:Roles:edit.html.twig', array(
            'entity'      => $entity,
            'edit_form'   => $editForm->createView(),
            'delete_form' => $deleteForm->createView(),
        ));
    }
    /**
     * Deletes a Roles entity.
     *
     */
    public function deleteAction(Request $request, $id)
    {
        $form = $this->createDeleteForm($id);
        $form->handleRequest($request);

        if ($form->isValid()) {
            $em = $this->getDoctrine()->getManager();
            $entity = $em->getRepository('PatoCoreBundle:Roles')->find($id);

            if (!$entity) {
                throw $this->createNotFoundException('Unable to find Roles entity.');
            }

            $em->remove($entity);
            $em->flush();
        }

        return $this->redirect($this->generateUrl('roles'));
    }

    /**
     * Creates a form to delete a Roles entity by id.
     *
     * @param mixed $id The entity id
     *
     * @return \Symfony\Component\Form\Form The form
     */
    private function createDeleteForm($id)
    {
        return $this->createFormBuilder()
            ->setAction($this->generateUrl('roles_delete', array('id' => $id)))
            ->setMethod('DELETE')
            ->add('submit', 'submit', array('label' => 'Delete'))
            ->getForm()
        ;
    }
}
