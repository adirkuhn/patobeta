<?php

namespace CloudSource\PatoCoreBundle\Controller;

use CloudSource\PatoCoreBundle\Helpers\JsonResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;

use CloudSource\PatoCoreBundle\Entity\States;
use CloudSource\PatoCoreBundle\Form\StatesType;

/**
 * States controller.
 *
 */
class StatesController extends Controller
{

    /**
     * Lists all States entities.
     *
     */
    public function indexAction()
    {
        $em = $this->getDoctrine()->getManager();

        $entities = $em->getRepository('PatoCoreBundle:States')->findAll();

        return $this->render('PatoCoreBundle:States:index.html.twig', array(
            'entities' => $entities,
        ));
    }
    /**
     * Creates a new States entity.
     *
     */
    public function createAction(Request $request)
    {
        $entity = new States();
        $form = $this->createCreateForm($entity);
        $form->handleRequest($request);

        if ($form->isValid()) {
            $em = $this->getDoctrine()->getManager();
            $em->persist($entity);
            $em->flush();

            return $this->redirect($this->generateUrl('states_show', array('id' => $entity->getId())));
        }

        return $this->render('PatoCoreBundle:States:new.html.twig', array(
            'entity' => $entity,
            'form'   => $form->createView(),
        ));
    }

    /**
    * Creates a form to create a States entity.
    *
    * @param States $entity The entity
    *
    * @return \Symfony\Component\Form\Form The form
    */
    private function createCreateForm(States $entity)
    {
        $form = $this->createForm(new StatesType(), $entity, array(
            'action' => $this->generateUrl('states_create'),
            'method' => 'POST',
        ));

        $form->add('submit', 'submit', array('label' => 'Create'));

        return $form;
    }

    /**
     * Displays a form to create a new States entity.
     *
     */
    public function newAction()
    {
        $entity = new States();
        $form   = $this->createCreateForm($entity);

        return $this->render('PatoCoreBundle:States:new.html.twig', array(
            'entity' => $entity,
            'form'   => $form->createView(),
        ));
    }

    /**
     * Finds and displays a States entity.
     *
     */
    public function showAction($id)
    {
        $em = $this->getDoctrine()->getManager();

        $entity = $em->getRepository('PatoCoreBundle:States')->find($id);

        if (!$entity) {
            throw $this->createNotFoundException('Unable to find States entity.');
        }

        $deleteForm = $this->createDeleteForm($id);

        return $this->render('PatoCoreBundle:States:show.html.twig', array(
            'entity'      => $entity,
            'delete_form' => $deleteForm->createView(),        ));
    }

    /**
     * Displays a form to edit an existing States entity.
     *
     */
    public function editAction($id)
    {
        $em = $this->getDoctrine()->getManager();

        $entity = $em->getRepository('PatoCoreBundle:States')->find($id);

        if (!$entity) {
            throw $this->createNotFoundException('Unable to find States entity.');
        }

        $editForm = $this->createEditForm($entity);
        $deleteForm = $this->createDeleteForm($id);

        return $this->render('PatoCoreBundle:States:edit.html.twig', array(
            'entity'      => $entity,
            'edit_form'   => $editForm->createView(),
            'delete_form' => $deleteForm->createView(),
        ));
    }

    /**
    * Creates a form to edit a States entity.
    *
    * @param States $entity The entity
    *
    * @return \Symfony\Component\Form\Form The form
    */
    private function createEditForm(States $entity)
    {
        $form = $this->createForm(new StatesType(), $entity, array(
            'action' => $this->generateUrl('states_update', array('id' => $entity->getId())),
            'method' => 'PUT',
        ));

        $form->add('submit', 'submit', array('label' => 'Update'));

        return $form;
    }
    /**
     * Edits an existing States entity.
     *
     */
    public function updateAction(Request $request, $id)
    {
        $em = $this->getDoctrine()->getManager();

        $entity = $em->getRepository('PatoCoreBundle:States')->find($id);

        if (!$entity) {
            throw $this->createNotFoundException('Unable to find States entity.');
        }

        $deleteForm = $this->createDeleteForm($id);
        $editForm = $this->createEditForm($entity);
        $editForm->handleRequest($request);

        if ($editForm->isValid()) {
            $em->flush();

            return $this->redirect($this->generateUrl('states_edit', array('id' => $id)));
        }

        return $this->render('PatoCoreBundle:States:edit.html.twig', array(
            'entity'      => $entity,
            'edit_form'   => $editForm->createView(),
            'delete_form' => $deleteForm->createView(),
        ));
    }
    /**
     * Deletes a States entity.
     *
     */
    public function deleteAction(Request $request, $id)
    {
        $form = $this->createDeleteForm($id);
        $form->handleRequest($request);

        if ($form->isValid()) {
            $em = $this->getDoctrine()->getManager();
            $entity = $em->getRepository('PatoCoreBundle:States')->find($id);

            if (!$entity) {
                throw $this->createNotFoundException('Unable to find States entity.');
            }

            $em->remove($entity);
            $em->flush();
        }

        return $this->redirect($this->generateUrl('states'));
    }

    /**
     * Creates a form to delete a States entity by id.
     *
     * @param mixed $id The entity id
     *
     * @return \Symfony\Component\Form\Form The form
     */
    private function createDeleteForm($id)
    {
        return $this->createFormBuilder()
            ->setAction($this->generateUrl('states_delete', array('id' => $id)))
            ->setMethod('DELETE')
            ->add('submit', 'submit', array('label' => 'Delete'))
            ->getForm()
        ;
    }

    /**
     * Return a list of states by the country
     *
     * @author Adir Kuhn <adirkuhn@gmail.com>
     *
     * @param Integer $country_id Id of the country
     *
     * @return Response States data by Country ID in JSON format
     */
    public function statesByCountryAction($country_id) {

        $em = $this->getDoctrine()->getManager();

        $statesEntities = $em->getRepository('PatoCoreBundle:States')->findBy(array('country' => $country_id ));
        $states = array();

        foreach($statesEntities as $stateEntity) {
            $states[] = array(
                'id' => $stateEntity->getId(),
                'state' => $stateEntity->getState()
            );
        }

        return JsonResponse::response($states, 200);
    }
}
