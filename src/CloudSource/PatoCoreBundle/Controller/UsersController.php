<?php

namespace CloudSource\PatoCoreBundle\Controller;

use CloudSource\PatoCoreBundle\Interfaces\InitializableControllerInterface;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;

use CloudSource\PatoCoreBundle\Entity\Users;
use CloudSource\PatoCoreBundle\Form\UsersType;

/**
 * Users controller.
 *
 */
class UsersController extends Controller implements InitializableControllerInterface
{

    //Name of the resource to protect
    private $aclProtectedResource = 'USERS';

    private $aclService = null;

    public function initialize() {
        //get aclService instance and register this resource
        $this->aclService = $this->get('PatoCoreBundle.AclService');

        $this->aclService->addProtectedResource($this->aclProtectedResource);
    }

    /**
     * @param $user
     * @param $plainTextPassword
     * @return mixed
     */
    private function encodePassword($user, $plainTextPassword) {
        $encoder = $this->get('security.encoder_factory')->getEncoder($user);

        return $encoder->encodePassword($plainTextPassword, $user->getSalt());
    }

    /**
     * Lists all Users entities.
     *
     */
    public function indexAction()
    {
        $this->aclService->isGranted($this->aclProtectedResource, $this->aclService->READ);

        $em = $this->getDoctrine()->getManager();

        $entities = $em->getRepository('PatoCoreBundle:Users')->findAll();

        return $this->render('PatoCoreBundle:Users:index.html.twig', array(
            'entities' => $entities,
        ));
    }
    /**
     * Creates a new Users entity.
     *
     */
    public function createAction(Request $request)
    {
        $this->aclService->isGranted($this->aclProtectedResource, $this->aclService->WRITE);

        $entity = new Users();
        $form = $this->createCreateForm($entity);
        $form->handleRequest($request);

        if ($form->isValid()) {
            $em = $this->getDoctrine()->getManager();

            //set encoded password
            $entity->setPassword($this->encodePassword($entity, $entity->getPassword()));

            $em->persist($entity);
            $em->flush();

            return $this->redirect($this->generateUrl('users_show', array('id' => $entity->getId())));
        }

        return $this->render('PatoCoreBundle:Users:new.html.twig', array(
            'entity' => $entity,
            'form'   => $form->createView(),
        ));
    }

    /**
    * Creates a form to create a Users entity.
    *
    * @param Users $entity The entity
    *
    * @return \Symfony\Component\Form\Form The form
    */
    private function createCreateForm(Users $entity)
    {
        $form = $this->createForm(new UsersType(), $entity, array(
            'action' => $this->generateUrl('users_create'),
            'method' => 'POST',
        ));

        $form->add('submit', 'submit', array('label' => 'Create'));

        return $form;
    }

    /**
     * Displays a form to create a new Users entity.
     *
     */
    public function newAction()
    {
        $this->aclService->isGranted($this->aclProtectedResource, $this->aclService->WRITE);

        $entity = new Users();
        $form   = $this->createCreateForm($entity);

        return $this->render('PatoCoreBundle:Users:new.html.twig', array(
            'entity' => $entity,
            'form'   => $form->createView(),
        ));
    }

    /**
     * Finds and displays a Users entity.
     *
     */
    public function showAction($id)
    {
        $em = $this->getDoctrine()->getManager();

        $entity = $em->getRepository('PatoCoreBundle:Users')->find($id);

        if (!$entity) {
            throw $this->createNotFoundException('Unable to find Users entity.');
        }

        $deleteForm = $this->createDeleteForm($id);

        return $this->render('PatoCoreBundle:Users:show.html.twig', array(
            'entity'      => $entity,
            'delete_form' => $deleteForm->createView(),        ));
    }

    /**
     * Displays a form to edit an existing Users entity.
     *
     */
    public function editAction($id)
    {
        $em = $this->getDoctrine()->getManager();

        $entity = $em->getRepository('PatoCoreBundle:Users')->find($id);

        if (!$entity) {
            throw $this->createNotFoundException('Unable to find Users entity.');
        }

        $editForm = $this->createEditForm($entity);
        $deleteForm = $this->createDeleteForm($id);

        return $this->render('PatoCoreBundle:Users:edit.html.twig', array(
            'entity'      => $entity,
            'edit_form'   => $editForm->createView(),
            'delete_form' => $deleteForm->createView(),
        ));
    }

    /**
    * Creates a form to edit a Users entity.
    *
    * @param Users $entity The entity
    *
    * @return \Symfony\Component\Form\Form The form
    */
    private function createEditForm(Users $entity)
    {
        $form = $this->createForm(new UsersType(), $entity, array(
            'action' => $this->generateUrl('users_update', array('id' => $entity->getId())),
            'method' => 'PUT',
        ));

        $form->add('submit', 'submit', array('label' => 'Update'));

        return $form;
    }
    /**
     * Edits an existing Users entity.
     *
     */
    public function updateAction(Request $request, $id)
    {
        $this->aclService->isGranted($this->aclProtectedResource, $this->aclService->WRITE);

        $em = $this->getDoctrine()->getManager();

        $entity = $em->getRepository('PatoCoreBundle:Users')->find($id);

        //check if password is empty, so do not encrypt and update password
        $updatePassword = true;
        $requestBagArr = $request->request->get('cloudsource_patocorebundle_users');

        if (empty($requestBagArr['password']['first'])) {
            $requestBagArr['password']['first'] = $entity->getPassword();
            $requestBagArr['password']['second'] = $entity->getPassword();
            $request->request->set('cloudsource_patocorebundle_users', $requestBagArr);
            $updatePassword = false;
        }

        if (!$entity) {
            throw $this->createNotFoundException('Unable to find Users entity.');
        }

        $deleteForm = $this->createDeleteForm($id);
        $editForm = $this->createEditForm($entity);
        $editForm->handleRequest($request);

        if ($editForm->isValid()) {

            //if user post valid password, so updated
            if ($updatePassword) {
                $entity->setPassword($this->encodePassword($entity, $entity->getPassword()));
            }

            $em->flush();

            return $this->redirect($this->generateUrl('users_edit', array('id' => $id)));
        }

        return $this->render('PatoCoreBundle:Users:edit.html.twig', array(
            'entity'      => $entity,
            'edit_form'   => $editForm->createView(),
            'delete_form' => $deleteForm->createView(),
        ));
    }
    /**
     * Deletes a Users entity.
     *
     */
    public function deleteAction(Request $request, $id)
    {
        $this->aclService->isGranted($this->aclProtectedResource, $this->aclService->DELETE);

        $form = $this->createDeleteForm($id);
        $form->handleRequest($request);

        if ($form->isValid()) {
            $em = $this->getDoctrine()->getManager();
            $entity = $em->getRepository('PatoCoreBundle:Users')->find($id);

            if (!$entity) {
                throw $this->createNotFoundException('Unable to find Users entity.');
            }

            $em->remove($entity);
            $em->flush();
        }

        return $this->redirect($this->generateUrl('users'));
    }

    /**
     * Creates a form to delete a Users entity by id.
     *
     * @param mixed $id The entity id
     *
     * @return \Symfony\Component\Form\Form The form
     */
    private function createDeleteForm($id)
    {
        return $this->createFormBuilder()
            ->setAction($this->generateUrl('users_delete', array('id' => $id)))
            ->setMethod('DELETE')
            ->add('submit', 'submit', array('label' => 'Delete'))
            ->getForm()
        ;
    }
}
