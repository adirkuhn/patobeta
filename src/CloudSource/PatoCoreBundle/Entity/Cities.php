<?php

namespace CloudSource\PatoCoreBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * Cities
 */
class Cities
{
    /**
     * @var integer
     */
    private $id;

    /**
     * @var string
     */
    private $city;

    /**
     * @var \CloudSource\PatoCoreBundle\Entity\States
     */
    private $state;


    /**
     * Get id
     *
     * @return integer
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * To string
     *
     * @return string City name
     */
     public function __toString()
     {
         return $this->city;
     }


    /**
     * Set city
     *
     * @param string $city
     * @return Cities
     */
    public function setCity($city)
    {
        $this->city = $city;

        return $this;
    }

    /**
     * Get city
     *
     * @return string
     */
    public function getCity()
    {
        return $this->city;
    }

    /**
     * Set state
     *
     * @param \CloudSource\PatoCoreBundle\Entity\States $state
     * @return Cities
     */
    public function setState(\CloudSource\PatoCoreBundle\Entity\States $state = null)
    {
        $this->state = $state;

        return $this;
    }

    /**
     * Get state
     *
     * @return \CloudSource\PatoCoreBundle\Entity\States
     */
    public function getState()
    {
        return $this->state;
    }
}
