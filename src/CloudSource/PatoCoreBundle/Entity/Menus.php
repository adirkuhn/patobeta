<?php

namespace CloudSource\PatoCoreBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * Menus
 */
class Menus
{
    /**
     * @var integer
     */
    private $id;

    /**
     * @var string
     */
    private $name;

    /**
     * @var string
     */
    private $path;

    /**
     * @var integer
     */
     private $ordenation;

    /**
     * @var string
     */
     private $icon;

    /**
     * Get id
     *
     * @return integer
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set name
     *
     * @param string $name
     * @return Menus
     */
    public function setName($name)
    {
        $this->name = $name;

        return $this;
    }

    /**
     * Get name
     *
     * @return string
     */
    public function getName()
    {
        return $this->name;
    }

    /**
     * Set path
     *
     * @param string $path
     * @return Menus
     */
    public function setPath($path)
    {
        $this->path = $path;

        return $this;
    }

    /**
     * Get path
     *
     * @return string
     */
    public function getPath()
    {
        return $this->path;
    }

    /**
     * Get ordenation
     *
     * @return integer
     */
    public function getOrdenation()
    {
        return $this->ordenation;
    }

    /**
     * Set ordenation
     *
     * @param string $ordenation
     * @return Menus
     */
    public function setOrdenation($ordenation)
    {
        $this->ordenation = $ordenation;

        return $this;
    }

    /**
     * Set font-awesome class icon
     *
     * @param String $icon Icon class name for menu
     *
     * @return Menus
     */
     public function setIcon($icon)
     {
         $this->icon = $icon;
     }

    /**
     * Get font-awesome class icon
     *
     *
     * @return String Icon class name
     */
     public function getIcon()
     {
         return $this->icon;
     }
}
