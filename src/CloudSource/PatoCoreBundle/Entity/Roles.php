<?php

namespace CloudSource\PatoCoreBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * Groups
 */
class Roles
{

    /**
     * @var integer
     */
    private $id;

    /**
     * @var string
     */
    private $permission;

    /**
     * @var \CloudSource\PatoCoreBundle\Entity\Users
     */
    private $user;

    /**
     * @var \CloudSource\PatoCoreBundle\Entity\AclProtected
     */
    private $aclProtected;


    /**
     * Get id
     *
     * @return integer 
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set permission
     *
     * @param string $permission
     * @return Roles
     */
    public function setPermission($permission)
    {
        $this->permission = $permission;

        return $this;
    }

    /**
     * Get permission
     *
     * @return string 
     */
    public function getPermission()
    {
        return $this->permission;
    }

    /**
     * Set user
     *
     * @param \CloudSource\PatoCoreBundle\Entity\Users $user
     * @return Roles
     */
    public function setUser(\CloudSource\PatoCoreBundle\Entity\Users $user = null)
    {
        $this->user = $user;

        return $this;
    }

    /**
     * Get user
     *
     * @return \CloudSource\PatoCoreBundle\Entity\Users 
     */
    public function getUser()
    {
        return $this->user;
    }

    /**
     * Set aclProtected
     *
     * @param \CloudSource\PatoCoreBundle\Entity\AclProtected $aclProtected
     * @return Roles
     */
    public function setAclProtected(\CloudSource\PatoCoreBundle\Entity\AclProtected $aclProtected = null)
    {
        $this->aclProtected = $aclProtected;

        return $this;
    }

    /**
     * Get aclProtected
     *
     * @return \CloudSource\PatoCoreBundle\Entity\AclProtected 
     */
    public function getAclProtected()
    {
        return $this->aclProtected;
    }

    public function __toString() {
        return $this->permission;
    }
}
