<?php

namespace CloudSource\PatoCoreBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * States
 */
class States
{
    /**
     * @var integer
     */
    private $id;

    /**
     * @var string
     */
    private $state;

    /**
     * @var \CloudSource\PatoCoreBundle\Entity\Countries
     */
    private $country;


    /**
     * Get id
     *
     * @return integer
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set state
     *
     * @param string $state
     * @return States
     */
    public function setState($state)
    {
        $this->state = $state;

        return $this;
    }

    /**
     * Get state
     *
     * @return string
     */
    public function getState()
    {
        return $this->state;
    }

    /**
     * Set country
     *
     * @param \CloudSource\PatoCoreBundle\Entity\Countries $country
     * @return States
     */
    public function setCountry(\CloudSource\PatoCoreBundle\Entity\Countries $country = null)
    {
        $this->countries = $country;

        return $this;
    }

    /**
     * Get country
     *
     * @return \CloudSource\PatoCoreBundle\Entity\Countries
     */
    public function getCountry()
    {
        return $this->country;
    }

    /**
     * To String
     */
     public function __toString()
     {
         return $this->state;
     }
}
