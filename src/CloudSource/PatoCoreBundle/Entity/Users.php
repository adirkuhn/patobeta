<?php

namespace CloudSource\PatoCoreBundle\Entity;

use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\Security\Core\User\UserInterface;
use Doctrine\Common\Collections\Collection;
use Doctrine\Common\Collections\ArrayCollection;

/**
 * Users
 */
class Users implements UserInterface
{
    /**
     * @var integer
     */
    private $id;

    /**
     * @var string
     */
    private $name;

    /**
     * @var string
     */
    private $email;

    /**
     * @var string
     */
    private $password;

    /**
     * @var string
     */
    private $salt;

    /**
     * @var boolean
     */
    private $isActive;

    /**
     * @var boolean
     */
    private $isAdmin;

    /**
     * @var \Doctrine\Common\Collections\Collection
     */
    private $roles;


    public function __construct()
    {
          $this->salt = base_convert(sha1(uniqid(mt_rand(), true)), 16, 36);
          $this->roles = new ArrayCollection();
    }

    public function __toString() {
        return $this->name;
    }

    /**
     * Get id
     *
     * @return integer
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set name
     *
     * @param string $name
     * @return Users
     */
    public function setName($name)
    {
        $this->name = $name;

        return $this;
    }

    /**
     * Get name
     *
     * @return string
     */
    public function getName()
    {
        return $this->name;
    }

    /**
     * Set email
     *
     * @param string $email
     * @return Users
     */
    public function setEmail($email)
    {
        $this->email = $email;

        return $this;
    }

    /**
     * Get email
     *
     * @return string
     */
    public function getEmail()
    {
        return $this->email;
    }

    /**
     * Set password
     *
     * @param string $password
     * @return Users
     */
    public function setPassword($password)
    {
        $this->password = $password;

        return $this;
    }

    /**
     * Get password
     *
     * @return string
     */
    public function getPassword()
    {
        return $this->password;
    }

    /**
     * Set salt
     *
     * @param string $salt
     * @return Users
     */
    public function setSalt($salt)
    {
        $this->salt = $salt;

        return $this;
    }

    /**
     * Get salt
     *
     * @return string
     */
    public function getSalt()
    {
        return $this->salt;
    }

    /**
     * Set isActive
     *
     * @param boolean $isActive
     * @return Users
     */
    public function setIsActive($isActive)
    {
        $this->isActive = $isActive;

        return $this;
    }

    /**
     * Get isActive
     *
     * @return boolean
     */
    public function getIsActive()
    {
        return $this->isActive;
    }

    /**
     * Get isAdmin
     *
     * @return boolean
     */
    public function getIsAdmin()
    {
        return $this->isAdmin;
    }

    /**
     * Set isAdmin
     *
     * @param boolean $isAdmin
     * @return Users
     */
    public function setIsAdmin($isAdmin)
    {
        $this->isAdmin = $isAdmin;

        return $this;
    }



    /**
     * Get Roles
     *
     */
    public function getRoles()
    {
        //default role
        $arrRoles = array('ROLE_USER');

        //user is admin? ROLE_ADMIN (no more roles needed)
        if ($this->isAdmin) {
            $arrRoles[] = 'ROLE_ADMIN';

            return $arrRoles;
        }

        //entity roles to array
        foreach($this->roles as $userRole) {
            $arrRoles[] = strtoupper('ROLE_' . $userRole->getAclProtected()->getName() . '_' . $userRole->getPermission());
        }

        return $arrRoles;
    }

    public function getRolesObj() {
        return $this->roles;
    }

    /**
     * Get Username
     *
     */
    public function getUsername()
    {
        return $this->email;
    }

    public function eraseCredentials()
    {
        return false;
    }


    /**
     * Add roles
     *
     * @param \CloudSource\PatoCoreBundle\Entity\Roles $roles
     * @return Users
     */
    public function addRole(\CloudSource\PatoCoreBundle\Entity\Roles $roles)
    {
        $this->roles[] = $roles;

        return $this;
    }

    /**
     * Remove roles
     *
     * @param \CloudSource\PatoCoreBundle\Entity\Roles $roles
     */
    public function removeRole(\CloudSource\PatoCoreBundle\Entity\Roles $roles)
    {
        $this->roles->removeElement($roles);
    }

    public function getRolesByAclProtectedResource($aclProtectedResource) {

        $roles = array();
        foreach($this->roles as $role) {
            if ($role->getAclProtected()->getName() === $aclProtectedResource)
                $roles[$role->getPermission()] = $role->getPermission();
        }
        return $roles;
    }
}
