<?php

namespace CloudSource\PatoCoreBundle\EventListener;

use CloudSource\PatoCoreBundle\Interfaces\InitializableControllerInterface;
use Symfony\Component\HttpKernel\Event\FilterControllerEvent;
use Symfony\Component\Security\Core\SecurityContextInterface;
use Symfony\Bundle\TwigBundle\Controller\ExceptionController;

/**
 * Class BeforeController
 * @package CloudSource\PatoCoreBundle\EventListener
 *
 * Listen Kernel events and run initialize method form
 */
class BeforeController
{
    public function onKernelController(FilterControllerEvent $event)
    {
        $controller = $event->getController();

        if (!is_array($controller)) {
            // not a object but a different kind of callable. Do nothing
            return;
        }

        $controllerObject = $controller[0];

        // skip initializing for exceptions
        if ($controllerObject instanceof ExceptionController) {
            return;
        }

        if ($controllerObject instanceof InitializableControllerInterface) {
            // this method is the one that is part of the interface.
            $controllerObject->initialize();
        }
    }
}