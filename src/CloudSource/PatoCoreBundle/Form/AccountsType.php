<?php

namespace CloudSource\PatoCoreBundle\Form;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolverInterface;

class AccountsType extends AbstractType
{
        /**
     * @param FormBuilderInterface $builder
     * @param array $options
     */
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add('name')
            ->add('corporateName')
            ->add('email')
            ->add('website')
            ->add('address')
            ->add('addressComplement')
        ;
    }
    
    /**
     * @param OptionsResolverInterface $resolver
     */
    public function setDefaultOptions(OptionsResolverInterface $resolver)
    {
        $resolver->setDefaults(array(
            'data_class' => 'CloudSource\PatoCoreBundle\Entity\Accounts'
        ));
    }

    /**
     * @return string
     */
    public function getName()
    {
        return 'cloudsource_patocorebundle_accounts';
    }
}
