<?php

namespace CloudSource\PatoCoreBundle\Form;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\Form\FormEvent;
use Symfony\Component\Form\FormEvents;
use Symfony\Component\OptionsResolver\OptionsResolverInterface;

class ContactsType extends AbstractType
{
        /**
     * @param FormBuilderInterface $builder
     * @param array $options
     */
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add('name')
            ->add('email')
            ->add('phone')
            ->add('cellphone')
            ->add('address')
            ->add('addressComplement')
            ->add('zip')
            ->add('description')
            //->add('city')
        ;

        //Chained Select Country / State / City
        $builder->add('country', 'entity', array(
            'class' => 'PatoCoreBundle:Countries',
            'required' => false,
            'mapped' => false,
            'empty_value' => 'Select a Country',
            'attr' => array('onchange' => 'loadStates(this)'),
        ));
        $builder->add('state', 'entity', array(
            'class' => 'PatoCoreBundle:States',
            'data' => null,
            'required' => false,
            'mapped' => false,
            'empty_value' => 'Select a Country first',
            'attr' => array('disabled' => 'disabled', 'onchange' => 'loadCities(this)'),
        ));
        $builder->add('city', 'entity', array(
            'class' => 'PatoCoreBundle:Cities',
            'required' => false,
            'empty_value' => 'Select a State first',
            'attr' => array('disabled' => 'disabled'),
        ));


        //event listener to load country and state on edit
        $builder->addEventListener(FormEvents::POST_SET_DATA, function(FormEvent $event) {
            $form = $event->getForm();
            $city = $event->getData()->getCity();

            if ($city !== null) {

                $form->remove('country');
                $form->remove('state');
                $form->remove('city');

                $form->add('country', 'entity', array(
                    'data' => $city->getState()->getCountry(),
                    'class' => 'PatoCoreBundle:Countries',
                    'required' => false,
                    'mapped' => false,
                    'empty_value' => 'Select a Country',
                    'attr' => array('onchange' => 'loadStates(this)'),
                ));
                $form->add('state', 'entity', array(
                    'class' => 'PatoCoreBundle:States',
                    'data' => $city->getState(),
                    'required' => false,
                    'mapped' => false,
                    'empty_value' => 'Select a Country first',
                    'attr' => array('disabled' => 'disabled', 'onchange' => 'loadCities(this)'),
                ));
                $form->add('city', 'entity', array(
                    'class' => 'PatoCoreBundle:Cities',
                    'data' => $city,
                    'required' => false,
                    'empty_value' => 'Select a State first',
                    'attr' => array('disabled' => 'disabled'),
                ));

            }
        });
    }

    /**
     * @param OptionsResolverInterface $resolver
     */
    public function setDefaultOptions(OptionsResolverInterface $resolver)
    {
        $resolver->setDefaults(array(
            'data_class' => 'CloudSource\PatoCoreBundle\Entity\Contacts'
        ));
    }

    /**
     * @return string
     */
    public function getName()
    {
        return 'cloudsource_patocorebundle_contacts';
    }
}
