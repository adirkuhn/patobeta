<?php

namespace CloudSource\PatoCoreBundle\Helpers;
use Symfony\Component\HttpFoundation\Response;


/**
 * Class JsonResponse
 * Helper to encode e return a response in JSON format
 * @package CloudSource\PatoCoreBundle\Helpers
 *
 * @author Adir Kuhn <adirkuhn@gmail.com>
 */
class JsonResponse {

    private $responseIns = null;

    /**
     * Return a response
     *
     * @autor Adir Kuhn <adirkuhn@gmail.com>
     *
     * @param array $payload Data to by encoded and sent
     * @param $responseStatusCode response status code
     */
    public static function response(array $payload, $responseStatusCode = 200) {
        return new Response(json_encode($payload), $responseStatusCode);
    }
}