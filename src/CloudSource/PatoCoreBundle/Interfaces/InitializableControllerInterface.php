<?php

namespace CloudSource\PatoCoreBundle\Interfaces;


interface InitializableControllerInterface
{
    public function initialize();
}