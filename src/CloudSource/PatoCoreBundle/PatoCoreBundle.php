<?php

namespace CloudSource\PatoCoreBundle;

use Symfony\Component\HttpKernel\Bundle\Bundle;

class PatoCoreBundle extends Bundle
{

    private $aclProtectedResources = array(
        'HOME',
        'CONFIGURATOR',
        'USERS',
        'CONTACTS',
        'ACCOUNTS'
    );

    private $menus = array(
        array('name' => 'Home', 'path' =>'pato_core_homepage', 'icon' => 'icon-dashboard' , 'ordenation' => 1),
        array('name' => 'Contacts', 'path' => 'contacts', 'icon' => 'icon-group' , 'ordenation' => 5),
        array('name' => 'Accounts', 'path' => 'accounts', 'icon' => 'icon-briefcase' , 'ordenation' => 10),
        array('name' => 'Cash Flow', 'path' => 'cashbook', 'icon' => 'icon-money' , 'ordenation' => 15),
        array('name' => 'Configuration', 'path' => 'configurator', 'icon' => 'icon-cogs' , 'ordenation' => 100),
    );

    private $aclService = null;
    private $menuService = null;

    public function boot() {

        $this->aclService = $this->container->get('PatoCoreBundle.AclService');

        $this->menuService = $this->container->get('PatoCoreBundle.MenuService');

        //Register Protect Resources
        foreach($this->aclProtectedResources as $aclProtectedResource) {
            $this->aclService->addProtectedResource($aclProtectedResource);
        }

        //Register Menus
        foreach($this->menus as $menu) {
            $this->menuService->registerMenu($menu['name'], $menu['path'], $menu['icon'], $menu['ordenation']);
        }
    }

}
