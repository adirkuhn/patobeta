<?php

namespace CloudSource\PatoCoreBundle\Services;

use CloudSource\PatoCoreBundle\Entity\AclProtected;
use Doctrine\ORM\EntityManager;
use Symfony\Component\Config\Definition\Exception\Exception;
use Symfony\Component\Security\Core\SecurityContext;
use Symfony\Component\Security\Core\Exception\AccessDeniedException;
/**
 * Acl Service
 * It add a resource to help define the ACL of a resource
 *
 * @author Adir Kuhn <adirkuhn@gmail.com>
 */
class AclService {

    public $READ = 'READ';
    public $WRITE = 'WRITE';
    public $DELETE = 'DELETE';

    private $securityContext = null;
    private $protectedResources = array();
    private $entityManager = null;


    /**
     * Initialize AclService
     *
     * @author Adir Kuhn <adirkuhn@gmail.com>
     *
     * @param SecurityContext $securityContext
     */
    public function __construct(SecurityContext $securityContext, EntityManager $entityManager) {

        //init vars
        $this->securityContext = $securityContext;
        $this->entityManager = $entityManager;

        //load resources from db
        $res = $this->entityManager->getRepository('PatoCoreBundle:AclProtected')->findAll();
        foreach($res as $resource) {
            $this->protectedResources[$resource->getName()] = $resource->getName();
        }
    }

    /**
     * Mount the name of role for access
     * e.g. RULE_HOME_READ / RULE_HOME_WRITE
     *
     * @author Adir Kuhn <adirkuhn@gmail.com>
     *
     * @param $aclProtectedResource Name of resource
     * @param $permission Permission of the Rule
     * @return string Rule Name
     */
    private function mountRoleName($aclProtectedResource, $permission) {
        return strtoupper('ROLE_' . $aclProtectedResource . '_' . $permission);
    }

    /**
     * Check if user is granted to access a resource
     *
     * @author Adir Kuhn <adirkuhn@gmail.com>
     *
     * @param $aclProtectedResource Name of resource
     * @return bool True if user has granted
     * @throws \Symfony\Component\Security\Core\Exception\AccessDeniedException
     */
    public function isGranted($aclProtectedResource, $rulePermission) {

        if ($this->securityContext->isGranted('ROLE_ADMIN')) {
            return true;
        }
        else if ($this->securityContext->isGranted($this->mountRoleName($aclProtectedResource, $rulePermission))) {
            return true;
        }
        else {
            throw new AccessDeniedException();
        }
    }

    /**
     * Add a resource to aclProtected
     *
     * @author Adir Kuhn <adirkuhn@gmail.com>
     *
     * @param string Name of resource
     */
     public function addProtectedResource($resource) {
         $resource = strtoupper($resource);
         if (!array_key_exists($resource, $this->protectedResources)) {

             $res = new AclProtected();
             $res->setName($resource);

             try {
                 $this->entityManager->persist($res);
                 $this->entityManager->flush();
             }
             catch(Exception $e) {
                 var_dump($e);
             }

             $this->protectedResources[$resource] = $resource;
        }
     }

     /**
      * Get resource by name
      *
      * @author Adir Kuhn <adirkuhn@gmail.com>
      *
      * @param string Name of resource
      *
      * @return string Name of resource
      */
      public function getProtectedResource($resource) {
          return $this->protectedResources[$resource];
      }

    /**
     * Get all resources
     *
     * @author Adir Kuhn <adirkuhn@gmail.com>
     *
     * @return mixed Array of all resources
     */
     public function getProtectedResources() {
         return $this->protectedResources;
     }

    public function getAllRoles() {

        $tmp = array();
        foreach($this->protectedResources as $protectedResource) {
            $tmp[strtoupper('ROLE_' . $protectedResource . '_READ')] = strtoupper('ROLE_' . $protectedResource . '_READ');
            $tmp[strtoupper('ROLE_' . $protectedResource . '_WRITE')] = strtoupper('ROLE_' . $protectedResource . '_WRITE');
            $tmp[strtoupper('ROLE_' . $protectedResource . '_DELETE')] = strtoupper('ROLE_' . $protectedResource . '_DELETE');
        }

        return $tmp;
    }

}
