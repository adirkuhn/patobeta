<?php

namespace CloudSource\PatoCoreBundle\Services;

use Doctrine\ORM\EntityManager;
use CloudSource\PatoCoreBundle\Entity\Menus;

/**
 * Menu Service
 *
 * Register and reder system menus
 */
class MenuService
{
    private $entityManager = null;
    private $menus = array();

    function __construct(EntityManager $entityManager)
    {
        $this->entityManager = $entityManager;

        //load menus from database
        $menus = $this->entityManager->getRepository('PatoCoreBundle:Menus')->findBy(array(), array('ordenation' => 'ASC'));
        foreach($menus as $menu) {
            $this->menus[$menu->getName()] = array('path' => $menu->getPath(), 'icon' => $menu->getIcon() );
        }
    }


    /**
     * Register a menu to app
     * @author Adir Kuhn <adirkuhn@gmail.com>
     *
     * @param string $menuName Name of menu
     * @param string $menuPath Path of menu (routing system)
     * @param int $menuOrder Order of menu
     *
     */
    public function registerMenu($menuName, $menuPath, $iconClassName, $menuOrdenation=null)
    {
        if (!array_key_exists($menuName, $this->menus)) {
            $menu = new Menus();
            $menu->setName($menuName);
            $menu->setPath($menuPath);
            $menu->setIcon($iconClassName);
            $menu->setOrdenation($menuOrdenation);

            $this->entityManager->persist($menu);
            $this->entityManager->flush();
        }
    }

    /**
     * Return menu list
     *
     * @author Adir Kuhn <adirkuhn@gmail.com>
     *
     * @return mixed Array of the menu
     */
     public function getMenus()
     {
         return $this->menus;
     }
}
